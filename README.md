# packed-ptr

Rust pointer type that allows packing additional data along with the underlying memory offset.
